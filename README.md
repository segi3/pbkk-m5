# Mengimplementasi Project Laravel 8
Oleh: Rafi Nizar Abiyyi / 05111840000094


## Project dibuat menggunakan composer

Composer akan secara otomatis mendownload repo laravel/laravel versi 8. Setelah berhasil membuat projek, selanjut nya membuat database mysql.

![1_1](doc_m5/1_1.PNG)

Membuat projek dengan composer

![1_2](doc_m5/1_2.PNG)

Membuat projek dengan composer

## Mengatur environment variable aplikasi

Env variabel disimpan dalam file .env pada root folder aplikasi, yang perlu dipastikan untuk pertama kali setelah membuat projek adalah variabel database.
Tipe database yang digunakan `mysql`, Host dan port database `localhost:3306`, nama database `pbkk-m5`, dan user database `root`.

![2_1](doc_m5/2_1.PNG)

Environment variabel pada `.env`

## Membuat scaffolding auth menggunakan jetstream

Laravel menyediakan scaffolding autentikasi menggunakan jetstream livewire untuk mempermudah menangani user login, reset password, two-factor authentication dengan membuatkan file dan konfigurasi yang dibutuhkan. 

![3_1](doc_m5/3_1.PNG)

Composer mengunduh laravel/jetstream

![3_2](doc_m5/3_2.PNG)

Menginstall jetstream livewire

![3_3](doc_m5/3_3.PNG)

Membuat aset dengan npm install && npm run dev

## Membuat tabel dengan migrations

Migrations adalah salah satu cara untuk membuat tabel dan merekam perubahan-perubahan yang terjadi pada tabel. Migrasi yang telah dibuat akan secara otomatis disimpan pada direktori `database/migrations`.

![4_1](doc_m5/4_1.PNG)

Membuat migration untuk tabel tasks

Dalam setiap file migrations, terdapat dua fungsi utama yaitu `up()` dan `down()`. Fungsi up adalah fungsi yang bertanggung jawab saat perintah php artisan migrate dijalankan. Kebalikannya, fungsi `down()` bertanggung jawab saat akan melakukan rollback pada migrations yang sudah dijalankan. Oleh karena itu ketika sebuah tabel atau kolom dibentuk pada fungsi `up()`, maka pada fungsi `down()` berisi menghapus tabel atau kolom tersebut.

![4_2](doc_m5/4_2.PNG)

File migration untuk tabel tasks

Setelah file migrations selesai dibuat, migrasi dilakukan dengan perintah `php artisan migrate`.

![4_3](doc_m5/4_3.PNG)

Definisi relasi pada model user

## Membuat model eloquent

Model merepresentasi entitas database dan dapat digunakan untuk melakukan query pada tabel. Setiap tabel pada database dapat memiliki satu model eloquent.

![5_1](doc_m5/5_1.PNG)

Membuat model untuk tabel Tasks

Perintah tersebut akan membuat file baru pada `app/models`.
Model juga dapat mendefinisikan relasi dengan model lain.

![5_2](doc_m5/5_2.PNG)

Definisi relasi pada model task

![5_2](doc_m5/5_2.PNG)

Definisi relasi pada model user

## Membuat controller

Controller digunakan untuk mengarahkan request antara views dan model. Controller disimpan dalam direktori `app/Http/Controllers`.

![6_1](doc_m5/6_1.PNG)

Membuat controller TaskController

## Membuat routing web

Routing berfungsi untuk menerima request dan mengarahkan request tersebut ke fungsi yang sesuai. Routing aplikasi disimpan dalam direktori `routes/web.php` sedangkan untuk api disimpan dalam `routes/api.php`.

Dengan menggunakan middleware auth dan verified, routing dapat dikelompokkan agar route hanya dapat diakses oleh user yang telah di verifikasi dan sudah login.

![7_1](doc_m5/7_1.PNG)

Routing untuk fitur tasks

## Merubah isi controller

Merubah isi controller agar memiliki fungsi-fungsi yang menjadi tujuan pada routing.

![8_1](doc_m5/8_1.PNG)

Mengubah TaskController

![8_2](doc_m5/8_2.PNG)

Mengubah TaskController

## Membuat views dengan blade

Membuat views sebagai html yang akan di render menggunakan blade engine. Views berfungsi untuk menampilkan html dengan data yang diberikan oleh controller.

![9_1](doc_m5/9_1.PNG)

Blade dashboard.blade.php

![9_2](doc_m5/9_2.PNG)

Blade add.blade.php

![9_3](doc_m5/9_3.PNG)

Blade edit.blade.php

## Menjalankan aplikasi pada localhost

Terakhir untuk mencoba aplikasi, laravel dapat di serve pada port 8000 secara default dengan perintah `php artisan serve`.

![10_8](doc_m5/10_8.PNG)

Menjalankan aplikasi menggunakan perintah `php artisan serve`

![10_1](doc_m5/10_1.PNG)

Halaman /register

![10_2](doc_m5/10_2.PNG)

Halaman /login

![10_3](doc_m5/10_3.PNG)

Halaman /dashboard

![10_4](doc_m5/10_4.PNG)

Halaman /task membuat task baru

![10_5](doc_m5/10_5.PNG)

Halaman  /dashboard setelah menambah task baru

![10_6](doc_m5/10_6.PNG)

Halaman /task/1 mengubah task dengan id 1

![10_7](doc_m5/10_7.PNG)

Halaman /dashboard setelah mengubah task
